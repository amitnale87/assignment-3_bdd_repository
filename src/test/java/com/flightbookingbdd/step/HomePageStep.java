
package com.flightbookingbdd.step;

import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;

import java.util.List;


import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class HomePageStep extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "list.depatureDate.homepage")
	private List<QAFWebElement> departureDateList;

	@FindBy(locator = "list.returningDate.homepage")
	private List<QAFWebElement> dreturnDateList;

	@QAFTestStep(description = "User opens Orbits site")
	public void luanchApplication() {
		// openPage(null, null);
		launchPage(null, null);
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		driver.manage().window().maximize();

	}

	@QAFTestStep(description = "User clicks on Flights tab")
	public void userClicksOnFlightsTab() {
		click("link.flightsLink.homepage");
	}

	@QAFTestStep(description = "User sends flying from city {0}")
	public void selectFromCity(String fromCity) {
		click("txt.flightOrigin.homepage");
		sendKeys(fromCity, "txt.flightOrigin.homepage");
		click("link.fromCity.homepage");
	}

	@QAFTestStep(description = "User sends flying to city {0}")
	public void selectToCity(String toCity) {
		click("txt.flightDestination.homepage");
		sendKeys(toCity, "txt.flightDestination.homepage");
		click("link.toCity.homepage");
	}

	@QAFTestStep(description = "User selects departure date")
	public void selectDepartureDate() {
		click("txt.flightDepating.homepage");
		departureDateList.get(0).click();
	}

	@QAFTestStep(description = "User selects return date")
	public void selectReturnDate() {
		click("txt.flightReturning.homepage");
		dreturnDateList.get(2).click();
	}

	@QAFTestStep(description = "User clicks on Search button")
	public void clickOnSearch() {
		click("btn.search.homepage");
	}

}
